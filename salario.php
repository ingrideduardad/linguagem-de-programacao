<?php

$salario = 0.0;

echo "Digite o salário:";
$salario = fgets(STDIN);
echo "\n";

if($salario < 1000.0){
    $salario = $salario + $salario*10/100; // aumento de 10%
}
else{
    if($salario > 2500.0){
        $salario = $salario*(1+3/100); // aumento de 3%
    }
    else{
        $salario = $salario*1.05; // aumento de 5%
    }
}

echo "Seu novo salário é de:".$salario."\n";

?>